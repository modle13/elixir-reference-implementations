#! /usr/bin/elixir

defmodule Mymodule do
    def some_function() do
        IO.puts "some_function was called"
    end
end

Mymodule.some_function()
