#! /usr/bin/elixir

result = ?a..?z

# can't print a range directly, have to convert
#
IO.puts(Enum.to_list(result))
