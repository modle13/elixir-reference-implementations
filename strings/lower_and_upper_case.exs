#! /usr/bin/elixir

# python equivalent
# "Hello World!".lower()
#
# upper case in elixir will be `upcase`
result = String.downcase("Hello World!")

IO.puts(result)
