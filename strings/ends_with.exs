#! /usr/bin/elixir

# python equivalent
# "Period".endswith(",")
# does python endswith accept a list?

result = String.ends_with?("Period!", [".", "?", "!"])

IO.puts(result)
