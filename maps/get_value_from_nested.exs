#! /usr/bin/elixir

nested = %{ one: %{ two: 7} }

result = get_in(nested, [:one, :two])
should_be_nil = get_in(nested, [:one, :three])

IO.puts(result)
IO.puts(should_be_nil)
IO.puts("")
