#! /usr/bin/elixir

# python equivalent
# "Hello World!"[0:1]

# TODO: figure out how to slice a range from a string
result = "Hello" <> " " <> "World"

IO.puts(result)
