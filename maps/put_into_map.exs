#! /usr/bin/elixir

nested = %{ one: %{ two: 7} }

# returns a copy of the map
modified_result = Map.put(nested, :b, 2)

result = modified_result.b

IO.puts(result)
