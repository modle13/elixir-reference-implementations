#! /usr/bin/elixir

defmodule Game do
    def run("start") do
        IO.puts "starting"
        run("getinput")
    end

    def run("exit") do
        IO.puts "exiting"
    end

    def run(command) do
        old_command = command
        new_command = IO.gets "What is your command "
        IO.puts "old command was #{old_command}"
        IO.puts "new command is #{new_command}"
        run(String.trim(new_command))
    end
end

Game.run("start")
